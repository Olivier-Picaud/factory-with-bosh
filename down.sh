#!/bin/bash

bosh -e aws -d concourse delete-deployment
bosh -e aws -d nexus delete-deployment
bosh -e awd -d sonarqube delete-deployment
bosh delete-env bosh-deployment/bosh.yml \
--ops-file bosh-deployment/aws/cpi.yml \
--ops-file bosh-deployment/external-ip-not-recommended.yml \
--state bosh-deployment/aws-state.json \
--vars-store bosh-deployment/aws-creds.yml \
--vars-file bosh-deployment/aws-vars.yml \
--var-file private_key=../../bosh.pem
