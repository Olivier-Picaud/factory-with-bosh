#!/bin/bash

bosh -n -e aws deploy -d concourse concourse-bosh-deployment/cluster/concourse.yml -l concourse-bosh-deployment/versions.yml \
--vars-store concourse-bosh-deployment/cluster/cluster-cred.yml -o concourse-bosh-deployment/cluster/operations/static-web.yml \
-o concourse-bosh-deployment/cluster/operations/basic-auth.yml --var local_user.username=admin \
--var local_user.password=admin --var web_ip=10.0.0.10 --var external_url=http://ec2-35-181-66-204.eu-west-3.compute.amazonaws.com:8080/ \
--var network_name=default --var web_vm_type=default --var db_vm_type=default --var db_persistent_disk_type=large \
--var worker_vm_type=large --var deployment_name=concourse

cd nexus-boshrelease
bosh -e aws create-release --name=nexus --force
bosh -n -e aws deploy -d nexus nexus.yml -v internal_ip=10.0.0.11
cd ..

cd sonarqube-bosh-release
bosh -e aws create-release --force
bosh -e aws upload-release && bosh -n -e aws deploy -d sonarqube manifest.yml
cd ..

cd influxdb-boshrelease
bosh -n -e aws deploy manifests/influxdb.yml -d influxdb -v internal_ip=10.0.0.13
cd ..

cd telegraf-boshrelease
bosh -e aws create-release --force && bosh -e aws upload-release
bosh -n -e aws deploy -d telegraf telegraf.yml \
-v influxdb.url=http://10.0.0.13:8086 \
-v internal_ip=10.0.0.14
cd ..

cd grafana-boshrelease
bosh -e aws create-release --force && bosh -e aws upload-release
bosh -n -e aws deploy -d grafana grafana.yml \
-v influxdb.url=http://10.0.0.13:8086 \
-v internal_ip=10.0.0.15
cd .. 
